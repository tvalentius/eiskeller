
import 'bootstrap/dist/css/bootstrap.css';
import '../stylesheets/main';



var Router = require('./router');
var CardActions = require('./actions/cardactions');

CardActions.fetchList(1);

Router.start();


// Service Worker for Caching

if ('serviceWorker' in navigator) {
	console.log("Service Worker Supported!");
	navigator.serviceWorker
				.register('./service-worker.js')
				.then(function() { console.log('Service worker registered') })
} else {
	console.log("Too Bad, SW not supported");
}