var React = require('react');
var Reflux = require('reflux');
var Row = require('react-bootstrap').Row;
var Card = require('./card');
var CardStore = require('../stores/cardstore');


var CardGrid = React.createClass({
	mixins: [Reflux.connect(CardStore, 'cardstore')],

    render: function() {
        console.log(this.state);
        if(this.state.cardstore) {
        	return (
    			<Row>
                    {this.state.cardstore.map(function (card) {
                        return (
                            <Card login={ card.login } 
                                    avatar={ card.avatar_url }
                                    url={ card.html_url } >        
                            </Card>
                        );
                    })}
                </Row>
        	);
        } else {
            return (<Row>Loading Data</Row>);
        }
    }
});

module.exports = CardGrid;



