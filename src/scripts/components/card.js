var React = require('react');
var Col = require('react-bootstrap').Col;

var Card = React.createClass({
	render: function() {
		return(
		<Col lg={4} md={6}>  
            <div className="github-card user-card">
                <div className="header">
                  <a className="avatar" href={ this.props.url } target="_top">
                    <img src={this.props.avatar}></img>
                    <strong>{ '@'+this.props.login }</strong>
                    <span></span>
                  </a>
                  <a className="button" href={ this.props.url } target="_top">Follow</a>
                </div>
                <ul className="status">
                  <li>
                    <a href={ this.props.url + "tab=repositories" } target="_top">
                      <strong></strong>Repos
                    </a>
                  </li>
                  <li>
                    <a href={"https://gist.github.com/" + this.props.login} target="_top">
                      <strong></strong>Gists
                    </a>
                  </li>
                  <li>
                    <a href={"https://github.com/" + this.props.login + "/followers"} target="_top">
                      <strong></strong>Followers
                    </a>
                  </li>
                </ul>
            </div>
        </Col>
        );
	}
})

module.exports = Card;