var React = require('react');
var CardGrid = require('./cardgrid');
var CardActions = require('../actions/cardactions');
var Pagination = require ('react-js-pagination');

var Home = React.createClass({
  
  getInitialState: function() {
    return {
      activePage: 1
    }
  },

  handlePageChange: function(pageNumber) {
    this.setState({activePage: pageNumber});
    CardActions.fetchList(pageNumber);
    console.log(`active page is ${pageNumber}`);
  },

  render: function() {

    return (
      <div className='container'>
        <CardGrid></CardGrid>
        
        <Pagination 
          activePage={this.state.activePage} 
      	  
          totalItemsCount={4500} 
          onChange={this.handlePageChange.bind(this)}
        />
      </div>
    );
  }
});

module.exports = Home;
