var Reflux = require('reflux');
var $ = require('jquery');
var CardActions = require('../actions/cardactions');
var githubClientId = '90967c4f9a19e1226c84';
var githubClientSecret = 'ab68c37f38aa76b7cdcd71748fa8c0b5c0f09903';

var CardStore = Reflux.createStore({
	listenables: [CardActions],
	cardlist: [],
	sourceUrl: 'https://api.github.com/users?client_id='+githubClientId+'&client_secret='+githubClientSecret+'&per_page=10&since=',

	init: function() {
		this.fetchList();
	},

	fetchList: function(pageNumber) {
		$.ajax({
			url:this.sourceUrl+(10*(pageNumber-1)),
			dataType: 'json',
			cache: false,
            context: this,
            success: function(data) {
                console.log('fetch complete');
                this.cardlist = data;
                this.trigger(this.cardlist);
            }
		})
		console.log('pageNumber',pageNumber);
	}

});

module.exports = CardStore;