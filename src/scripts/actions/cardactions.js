var Reflux = require('reflux');

var CardActions = Reflux.createActions([
	'fetchList'
]);

module.exports = CardActions;